#include "history.h"

void History::add_game(Match_up game)
{
	table_of_games.push_back(game);
}

void History::draw_table(int number_of_matches,int games_to_win,int number_of_teams)
{
	int px = 0;
	int py = 8;

	int new_column = number_of_teams / 2;
	int new_column_y = 8;

	cout << ("tabela rozgrywek:") << endl;

	for (int x = 1; x < number_of_matches + 1; x++)
	{
		table_of_games[x - 1].show_match_past(games_to_win, px, py);
		py += 5;

		if (x == new_column)
		{
			px += 40;
			py = new_column_y + number_of_teams / 2 + 1;
			new_column_y = py;
			number_of_teams /= 2;
			new_column += number_of_teams / 2;
		}
	}

}