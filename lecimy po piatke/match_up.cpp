#include "match_up.h"

Match_up::Match_up()
{
	match = new Team[2];
}

void gotoxy(const int x, const int y)
{
	COORD coord = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void Match_up::add_teams(Team one,Team two)
{
	match[0] = one;
	match[1] = two;
}

void Match_up::show_match(int max)
{
	cout << ("_____________________________________ ") << endl << endl;
	if ((match[0].wins_output()<max) && (match[1].wins_output() < max))
	{
		SetConsoleTextAttribute(hOut, BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
		cout << match[0].name_output() << ("(id:") << match[0].id_output() << ("): ") << match[0].wins_output() << endl;
		SetConsoleTextAttribute(hOut, BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
		cout << match[1].name_output() << ("(id:") << match[1].id_output() << ("): ") << match[1].wins_output() << endl;
	}
	else if ((match[0].wins_output() == max) && (match[1].wins_output() < max))
	{
		SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
		cout << match[0].name_output() << ("(id:") << match[0].id_output() << ("): ") << match[0].wins_output() << endl;
		SetConsoleTextAttribute(hOut, FOREGROUND_RED | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
		cout << match[1].name_output() << ("(id:") << match[1].id_output() << ("): ") << match[1].wins_output() << endl;
	}
	else if ((match[0].wins_output() < max) && (match[1].wins_output() == max))
	{
		SetConsoleTextAttribute(hOut, FOREGROUND_RED | BACKGROUND_BLUE | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
		cout << match[0].name_output() << ("(id:") << match[0].id_output() << ("): ") << match[0].wins_output() << endl;
		SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
		cout << match[1].name_output() << ("(id:") << match[1].id_output() << ("): ") << match[1].wins_output() << endl;
	}

	SetConsoleTextAttribute(hOut, BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
	cout << ("_____________________________________ ") << endl;
}

void Match_up::show_match_past(int max, int x, int y)
{
	if ((match[0].wins_output()<max) && (match[1].wins_output() < max))
	{
		SetConsoleTextAttribute(hOut, BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		gotoxy(x, y);
		cout << match[0].name_output() << ("(id:") << match[0].id_output() << ("): ") << match[0].wins_output();
		gotoxy(x, y + 1);
		cout << match[1].name_output() << ("(id:") << match[1].id_output() << ("): ") << match[1].wins_output();
	}
	else if ((match[0].wins_output() == max) && (match[1].wins_output() < max))
	{
		gotoxy(x, y);
		SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		cout << match[0].name_output() << ("(id:") << match[0].id_output() << ("): ") << match[0].wins_output();
		SetConsoleTextAttribute(hOut, FOREGROUND_RED | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		gotoxy(x, y + 1);
		cout << match[1].name_output() << ("(id:") << match[1].id_output() << ("): ") << match[1].wins_output();
	}
	else if ((match[0].wins_output() < max) && (match[1].wins_output() == max))
	{
		gotoxy(x, y);
		SetConsoleTextAttribute(hOut, FOREGROUND_RED | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		cout << match[0].name_output() << ("(id:") << match[0].id_output() << ("): ") << match[0].wins_output();
		SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		gotoxy(x, y + 1);
		cout << match[1].name_output() << ("(id:") << match[1].id_output() << ("): ") << match[1].wins_output();
	}

	SetConsoleTextAttribute(hOut, BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
}


void Match_up::search(int id,int max)
{
	if (active == true)
	{
		if (match[0].id_output() == id)
		{
			match[0].wins_increase();
			match[1].loses_increase();
		}

		else if (match[1].id_output() == id)
		{
			match[1].wins_increase();
			match[0].loses_increase();
		}

		if((match[0].wins_output() == max )||(match[1].wins_output() == max ))
		{
			active = false;
		}
	}
	

}

bool Match_up::end_of_game(int max)
{
		if (match[0].wins_output() == max)
		{
			winner_id = match[0].id_output();
			loser_id = match[1].id_output();
			return true;
		}

		else if (match[1].wins_output() == max)
		{
			winner_id = match[1].id_output();
			loser_id = match[0].id_output();
			return true;
		}

		return false;

}

int Match_up::loser_output()
{
	return loser_id;
}

int Match_up::winner_output()
{
	return winner_id;
}