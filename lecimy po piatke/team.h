#pragma once

#include <iostream>
#include <string>

using namespace std;

class Team
{
private:
	string name;
	int wins;
	int loses;
	int id;
	int can_play=1;

public:
	Team(string n = "no name", int w = 0, int l = 0, bool c = true);
	void input_name(string n);
	void wins_increase();
	void loses_increase();
	void id_input(int i);
	void cant_play();
	void canplay();
	string name_output();
	int wins_output();
	int loses_output();
	int id_output();
	int ask_can_play();
};
