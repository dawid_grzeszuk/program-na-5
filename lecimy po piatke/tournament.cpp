#include "tournament.h"

void Tournament::number_of_teams_input()
{
	int number=0;

	while (true)
	{
		cout << ("Aby rozpoczac podaj ilosc druzyn (potega dwojki,minimum 4): ");
		cin >> number;
		system("cls");
		if ((number == 4) || (number == 8) || (number == 16) || (number == 32) || (number == 64) || (number == 128))
		{
			number_of_teams = number;
			number_of_teams_playing = number;
			break;
		}
		else
		{
			cout << ("wprowadz potege dwojki") << endl << endl;
		}
	}
}

void Tournament::game_settings_input()
{
	cout << ("Prosze podaj maksymalna liczbe gier w turze: ");
	cin >> max_games;
	system("cls");
    games_to_win=max_games/2+1;

}


void Tournament::game_table_making()
{
	game_table = new Match_up[number_of_teams / 2];
	int counter = 0;
	int temp1=0;
	int temp2=0;

	int sw = 0;


	for (int x = 0; x < number_of_teams_playing; x++)
	{

		if ((list_of_teams[x].ask_can_play() == true) && (sw == 0))
		{
			temp1 = x;
			sw = 1;
		}

		if ((list_of_teams[x].ask_can_play() == true) && (sw == 1) && (x!=temp1))
		{
			temp2 = x;
			sw = 2;
		}

		if(sw==2)
		{
			game_table[counter].add_teams(list_of_teams[temp1],list_of_teams[temp2]);
			sw = 0;
			counter ++;
		}
	}

	show_list_of_teams();
}

void Tournament::teams_input_manual()
{
	string name;
	string *name_of_team = new string[number_of_teams];
	list_of_teams = new Team[number_of_teams];

	for (int x = 0; x < number_of_teams; x++)
	{
		system("cls");
		cout << ("podaj nazwe ") << x + 1 << (" druzyny: ");
		cin >> name;
		name_of_team[x]=name;
	}

	random_shuffle(name_of_team, name_of_team + number_of_teams);

	for (int x = 0; x<number_of_teams; x++)
	{
		list_of_teams[x].input_name(name_of_team[x]);
		list_of_teams[x].id_input(x + 1);
	}

	game_table_making();
	show_tournament();

}

void Tournament::teams_input_automatic()
{
	file.open("teams.txt", std::ios::in | std::ios::out);

	list_of_teams = new Team[number_of_teams];

	string line;
	string *name_of_team = new string[number_of_teams];

	int load_line = 0;

	if (file.good() == true)
	{
		while (getline(file, line))
		{
			if (load_line < number_of_teams)
			{
				name_of_team[load_line] = line;
				load_line++;
			}	
		}
		file.close();
	}

	random_shuffle(name_of_team,name_of_team+number_of_teams);

	for (int x = 0;x<number_of_teams; x++)
	{
		list_of_teams[x].input_name(name_of_team[x]);
		list_of_teams[x].id_input(x + 1);
	}

	game_table_making();
	show_tournament();
}

void Tournament::show_list_of_teams()
{
	int counter = 1;
	system("cls");

	cout << ("Oto lista druzyn ktora przeszla do dalszych rozgrywek: ") << endl << endl;


	for (int x = 0; x < number_of_teams_playing; x++)
	{
		if (list_of_teams[x].ask_can_play() == true)
		{
			cout << counter << (".") << list_of_teams[x].name_output()  << endl;
			counter++;
		}
	}

	cout << endl << endl;
	system("pause");
	system("cls");
}

void Tournament::show_tournament()
{
	system("cls");

	while (game==true)
	{
		cout << ("Oto lista druzyn: ") << endl;

		for (int x = 0; x < number_of_teams / 2; x++)
		{
			game_table[x].show_match(games_to_win);
		}
		add_win();
	}
}

void Tournament::add_win()
{
	int choice=0;
	cout << endl << ("podaj id wygranej druzyny: ");
	cin >> choice;

	for (int x = 0; x < number_of_teams / 2; x++)
	{
		game_table[x].search(choice, games_to_win);
	}

	system("cls");
	search_for_next();
}

void Tournament::search_for_next()
{
	bool accept = true;

	for (int x = 0; x < number_of_teams / 2; x++)
	{
		if (game_table[x].end_of_game(games_to_win) == false)
		{
			accept = false;
		}
	}

	if (accept == true)
	{
		for (int x = 0; x < number_of_teams/2; x++)
		{
				list_of_teams[game_table[x].loser_output() - 1].cant_play();
				past.add_game(game_table[x]);
				number_of_matches++;
		}

		number_of_teams = number_of_teams / 2;

		if (number_of_teams == 1)
		{
			cout << ("zakonczono rozgrywki !!!") << endl << endl;
			system("pause");
			game = false;
		}

		else  if(number_of_teams == 2)
		{
			cout << ("zakonczono ta ture rozgrywek !!!") << endl << endl;
			system("pause");
			finals();
		}

		else
		{
		   cout << ("zakonczono ta ture rozgrywek !!!") << endl << endl;
		   system("pause");
		   game_table_making();
		}
	}

}

void Tournament::finals()
{
	system("cls");
	int final1 = game_table[0].winner_output() - 1;
	int final2 = game_table[1].winner_output() - 1;
	int third1 = game_table[0].loser_output()-   1;
	int third2 = game_table[1].loser_output() -  1;

	list_of_teams[third1].canplay();
	list_of_teams[third2].canplay();

	game_table = new Match_up[2];
	game_table[0].add_teams(list_of_teams[final1], list_of_teams[final2]);
	game_table[1].add_teams(list_of_teams[third1], list_of_teams[third2]);

	while (true)
	{
		bool accept = true;

		for (int x = 0; x < 2; x++)
		{
			if (game_table[x].end_of_game(games_to_win) == false)
			{
				accept = false;
			}
		}

		if (accept == true)
		{
			break;
		}

		cout << ("Oto druzyny ktore walcza o pierwsze miejsce: ") << endl;

		game_table[0].show_match(games_to_win);
		cout << endl << ("Oto druzyny ktore walcza o trzecie miejsce: ") << endl;

		game_table[1].show_match(games_to_win);

		int choice = 0;
		cout << endl << ("podaj id wygranej druzyny: ");
		cin >> choice;

		for (int x = 0; x < 2; x++)
		{
			game_table[x].search(choice, games_to_win);
		}
		system("cls");
	}

	cout << ("Oto lista zajetych miejsc: ") << endl << endl;

	cout << "1." << list_of_teams[game_table[0].winner_output() - 1].name_output() << endl;
	cout << "2." << list_of_teams[game_table[0].loser_output() - 1].name_output() << endl;
	cout << "3." << list_of_teams[game_table[1].winner_output() - 1].name_output() << endl << endl;

	number_of_matches += 1;
	past.add_game(game_table[0]);
	past.draw_table(number_of_matches,games_to_win,number_of_teams_playing);

	cin >> help;
	system("cls");
	cout<<("Zakonczono turniej, dziekujemy za skorzystanie z naszego programu. Polecamy sie na przyszlosc :D ") << endl << endl;
	system("pause");
	exit(0);
}
