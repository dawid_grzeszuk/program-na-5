#pragma once

#include <iostream>
#include <fstream>
#include <string>


using namespace std;

class Load_from_file
{
private:
	fstream file;
	string *table_of_names;
public:
	void load(int number);
	string name_output(int number);
};