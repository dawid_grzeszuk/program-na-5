#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include "team.h"
#include "match_up.h"
#include "load_from_file.h"
#include "history.h"
#include <Windows.h>

using namespace std;

class Tournament
{
private:
	int max_games;
	int games_to_win;
	int number_of_teams;
	int number_of_teams_playing;
	int number_of_matches=0;
	string help;

	Team *list_of_teams;
	Match_up *game_table;
	History past;
	fstream file;
	bool game = true;

public:
	void number_of_teams_input();
	void game_settings_input();
	void game_table_making();
	void teams_input_manual();
	void teams_input_automatic();
	void show_list_of_teams();
	void show_tournament();
	void add_win();
	void search_for_next();
	void finals();
};