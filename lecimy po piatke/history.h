#pragma once

#include <iostream>
#include <vector>
#include "match_up.h"

using namespace std;

class History
{
private:
	vector < Match_up > table_of_games;
public:
	void add_game(Match_up game);
	void draw_table(int number_of_matches,int games_to_win,int number_of_teams);
};