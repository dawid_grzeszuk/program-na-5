#include "team.h"

Team::Team(string n, int w, int p, bool c)
{
	this->name = n;
	this->wins = w;
	this->loses = p;
	this->can_play= c;
}

void Team::input_name(string n)
{
	this->name = n;
}

void Team::wins_increase()
{
	wins++;
}

void Team::loses_increase()
{
	loses++;
}

int Team::wins_output()
{
	return wins;
}

int Team::loses_output()
{
	return loses;
}

string Team::name_output()
{
	return name;
}

void Team::id_input(int i)
{
	this->id = i;
}

int Team::id_output()
{
	return id;
}

void Team::cant_play()
{
	this->can_play = false;
}

int Team::ask_can_play()
{
	return can_play;
}

void Team::canplay()
{
	can_play = 1;
}