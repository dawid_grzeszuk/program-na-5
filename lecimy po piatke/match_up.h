#pragma once

#include <iostream>
#include <string>
#include <windows.h>
#include <cstdlib>
#include "team.h"

using namespace std;

class Match_up
	: public Team
{
private:
	Team *match;
	bool active=true;
	int loser_id = 0;
	int winner_id = 0;
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

public:
	Match_up();
	void add_teams(Team one, Team two);
	void show_match(int max);
	void show_match_past(int max, int x , int y);
	void search(int id,int max);
	int loser_output();
	int winner_output();
	bool end_of_game(int max);

};